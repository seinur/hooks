import React, {useState, useEffect, useContext} from 'react';
import ReactDOM from 'react-dom';

const {Provider, Consumer} = React.createContext();

const SubApp = () => {
  const value = useContext(Consumer);
  return(
    <h1>{value}</h1>
  )
}


const App = () => {
  const [color, setColor] = useState();
  const [hideAziret, setHideAziret] = useState(false);

  const styles ={
    padding:'20px',
    background: color,
  }

  const aziret = hideAziret ? null : <SubApp/>;

  return(
    <Provider value={'abcdefgh'}>
      <div style={styles}>
        {aziret}
        <input onClick={() => {setHideAziret((old_state) => {setHideAziret (!old_state)})}} type='button' value='Hide/Show'/>
        <input onClick={() => {setColor('red')}} type='button' value='Red'/>
        <input onClick={() => {setColor('green')}} type='button' value='Green'/>
      </div>
    </Provider>
  )
}


ReactDOM.render(
    <App />,
  document.getElementById('root')
);
